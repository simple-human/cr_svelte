# Control-room svelte frontend

[Control-room](https://gitlab.com/simple-human/control_room) is a general purpose monitoring library.

This is a client part. It includes the `cr.js` library to communicate with server and 
a number of default *observers* - [svelte](https://svelte.dev) components for data visualization.

## usage

  ```
  git clone https://gitlab.com/simple-human/cr_svelte dashboard
  # or if degit/tiget is installed
  # npx degit gitlab:simple-human/cr_svelte dashboard
  cd dashboard
  npm install
  npm run dev
  ```

For the `control_room` server implementation [cr_server](https://gitlab.com/simple-human/cr_server) can be used.

## cr.js

A small library to connect to *control_room* server.

An example of library usage in svelte component

```
<script>
import { connect, authenticate, authorization, connection, observables, shadow } from './cr.js';
import TextObserver from './TextObserver.svelte';
import SvgObserver from './SvgObserver.svelte';

let controls;
let text_input;

connect('wss://localhost:9001/control_room');
$: if($connection) authenticate("user", "password");
</script>

<main>
	<h1>Dashboard</h1>
    {#if $connection && $authorization }

    	<h2>Available observables: </h2>
    	<ul>
      	{#each Object.keys($observables) as obs}
    	<li>{obs}</li>
    	{/each}
    	</ul>

    	<h2>Simple text observable with controls</h2>
      <TextObserver observable="date" bind:controls={controls}/>
      <div use:shadow={{component: SvgObserver, props: {observable: "date"}}}/>
      <form>
          <label>
              Controls
              <input type="text" bind:value={text_input}/>
          </label>
          <button on:click|preventDefault={() => { controls = { controls: text_input }}} >Ok</button>
      </form>

    {/if} 
</main>
```

Here a set of functions and objects from `cr.js` is used:

- *connect(addr)* - makes a connection to websocket address, 
  has a default argument to \`wss://${location.hostname}:9001/control_room\`
- *authenticate(user, password)* - authenticate user with password
- *connection* - a boolean svelte store for connection status
- *authorization* - a boolean svelte store for authorization status
- *observables* - a svelte store for *observables* object from the server
  (see documentation of [control_room](https://gitlab.com/simple-human/control_room))
- *shadow*(node, args) - an auxilary function used as svelte action to hide a component in Shadow DOM.
  It's useful to avoid id collisions in SVG components for example.
  *args* object should contain *component* and *props* fields.

As an example, here is the `TextObserver` implementation

```
<script>
import {init_observer} from './cr.js';

// the observable to subscribe
export let observable;

// controls can come from component property or as a default from the server
export let controls = undefined;

let _controls;
let datasets;

let {config, update} = init_observer(observable);

$: if(controls) {
    _controls = controls;
} else if($config) {
    _controls = $config.client.controls;
}

$: if (_controls) {
    datasets = update(_controls);
}

</script>
{#if $datasets}
{#each $datasets as dataset,i}
    <h2>{$config.data_conf[i].name}</h2>
    <p>
        {dataset}
    </p>
{/each}
{/if}
```

the `init_observer(observable)` function returns an object with observable `config` and `update(controls)` function.

The config object has 

- optional *title*;
- optional *description*;
- optional *options*, object to configure client side rendering;
- optional *controls*, a suggestion for client for default observable controls.

The `update(controls)` function subscribes to an observable at first call and update controls at next calls.
It returns a svelte store that updates to `datasets` array from server.
The structure of dataset depends on observable.

There is a set of observer components provided with the library:

- *TextObserver.svelte* - the simplest observer to display text info, 
  can be useful to display text messages, logs, etc. It accepts string datasets;
- *HtmlObserver.svelte* - this observer uses a handlebars html template for data visualisation,
  the *options* config contains *template* string.
- *PlotObserver.svelte* - a plotter observer based on [chart.js](https://chartjs.org).
  Config *options* object is used as chartjs plot options.
- *SvgObserver.wc.svelte* - this is an example of svg observer component. It uses shadow DOM to avoid ID collisionin SVG components for example. *args* is an object with *component an