import { readable, writable, derived, get } from 'svelte/store';
import { onMount, onDestroy } from 'svelte';

//let socket = new WebSocket('wss://' + location.hostname + ':9001')

let socket;

export let connection = writable(false);
export let authorization = writable(false);

connection.subscribe(() => authorization.set(false))

let max_id = 0;


let observers = {};
export let observables = writable({});

export function connect(addr=('wss://' + location.hostname + ':9001/control_room')) {

    if (socket) socket.close();

    socket = new WebSocket(addr);
    socket.onmessage = (m) => {
        //console.log("MSG: " + m.data)
        let msg = JSON.parse(m.data)
        let command = Object.keys(msg)[0]
        let data = msg[command]
        switch (command) {
            case "auth":
                authorization.set(data);
                break;
            case "observables":
                observables.set(data);
                break;
            case "data":
                observers[data.receiver].set(
                    {
                        load: data.load, 
                        dataset: data.dataset
                    }
                );
                break;
        }
    }


    socket.onopen = () => {
        connection.set(true)
    }

    socket.onclose = () => {
        connection.set(false)
    }
}

export function authenticate(login, passwd) {
    if(!socket) {
        console.log("connect to server first!");
        return;
    }

    let packed = JSON.stringify({
      'Auth': {
        'user': login,
        'password': passwd
      }
    });
    socket.send(packed);
}

function subscribe (id, obs, controls) {
    let packed = JSON.stringify({
      'Subscribe': {
        'subscriber': id,
        'observable': obs,
        'controls': controls
      }
    })
    socket.send(packed);
    observers[id] = writable(undefined)

    return observers[id]
  }

function update_controls (id, controls) {
    let packed = JSON.stringify({
      'Controls': {
        'observable_id': id,
        'controls': controls
      }
    })
    socket.send(packed);
  }

function _unsubscribe (id) {
    let packed = JSON.stringify({
        'Unsubscribe': id
    })
    socket.send(packed)
}

export function init_observer(observable) {

    if(!get(connection)) {
        console.log("connect to server first!");
        return;
    }

    if(!get(authorization)) {
        console.log("login first!");
        return;
    }

    let id = max_id;
    max_id++;

    let datasets;
    let config = derived(observables, (observables) => observables[observable]) 

    const update = function (controls={}) {


        if (datasets) {
            update_controls(id, controls);
        } else {
            let messages = subscribe(id, observable, controls);

            datasets = derived(messages, (msg,set) => { 
                if (msg) {
                    let data = get(datasets);
                    data[msg.dataset] = msg.load;
                    set(data);
                }
            }, [])



            onDestroy(() => {
                _unsubscribe(id)
            })
        }

        return datasets
    }

    const unsubscribe = function() {
        console.log("there");
        _unsubscribe(id)
    }

    return {config, update, unsubscribe};
}

export function shadow(node, args) {
  let root = node.attachShadow({mode: 'open'});

  let c = new args.component({
    target: root,
    props: args.props,
  });

  return {
    destroy: () => {
        c.$destroy()
    }
  }
}
